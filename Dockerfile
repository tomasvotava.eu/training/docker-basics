FROM python:3.9-slim

WORKDIR /code

ADD src/* ./

RUN pip install -r requirements.txt

CMD python -m uvicorn main:app --port 3000 --host 0.0.0.0

